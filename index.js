const express = require('express');
const cors = require('cors');
//login 기능 추가를 위한 import
const passport = require('passport');
const session = require('express-session');
const LocalStrategy = require('passport-local').Strategy;

const app = express();

app.use(express.json());

// CORS 예외처리를 위해 추가
let corsOptions = {
  origin: "*",        //출처 허용 옵션
  credential: true,   //사용자 인증이 필요한 리소스(쿠키 등) 접근
}
app.use(cors(corsOptions));

// express 세션 사용
app.use(session({ secret: "your-secret-key", resave: true, saveUninitialized: true }));

// passport 초기화
app.use(passport.initialize());
app.use(passport.session());

// post route
const postsRouter = require('./routes/posts');
app.use('/posts', postsRouter);


const port = 18080;
app.listen(port, () => {
  console.log(`Server is running on port ${port} 🚗`);
});