const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const router = express.Router();
const { connect, Request } = require('mssql');
const dbConfig = require('../dbConfig');

//
passport.use(
    new LocalStrategy(async (userId, password, done) => {
        try{
            const pool = await connect(dbConfig);
            const request = new Request(pool);
            request.input('userId', userId);
            request.input('password', password);

            const query = "SELECT * FROM TB_ADM_USER_MST WHERE USER_ID = @userId AND PASSWORD = @password";
            const result = await request.query(query);

            if (result.recordset.length > 0) {
                const user = result.recordset[0];
                return done(null, user);
            } else {
                return done(null, false, { message: "Invalid id or password. So sad😥" });
            } 
        } catch(err) {
            return done(err);
        }
    })
);

passport.serializeUser((user, done) => {
    done(null, user.USER_ID);
});

passport.deserializeUser(async (userId, done) => {
    try{
        const pool = await connect(dbConfig);
        const request = new Request(pool);
        request.input('userId', userId);

        const query = "SELECT * FROM TB_ADM_USER_MST WHERE USER_ID = @userId";
        const result = await request.query(query);

        if(result.recordset.length > 0){
            const user = result.recordset[0];
            return done(null, user);
        } else {
            return done(null, false, { message: "Invalid user. so sad" });
        } 
    } catch(err) {
        return done(err);
    }
});

// 로그인
router.post('/login', passport.authenticate('local'), (req, res) => {
    res.json({ message: "Login success.", user: req.user });
});

// 로그아웃
router.get('/logout', (req, res) => {
    req.logout();
    res.json({ message: "Logout success." });
});

module.exports = router;