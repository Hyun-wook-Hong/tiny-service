const express = require('express');
const router = express.Router();
const { connect, Request } = require('mssql');
const dbConfig = require('../dbConfig');
const queries = require('../queries');
const { Router } = require('express');

// Get all posts
router.get('/', async (req, res) => {
  try {
    console.log("GET: View all post.");
    const pool = await connect(dbConfig);
    const request = new Request(pool);
    const result = await request.query(queries.getAllPostsQuery);
    res.json(result.recordset);
  } catch (err) {
    console.error('Database connection failed:', err); 
    res.status(500).send('Database connection failed');
  }
  finally
  {
    request.close();
  }
});

// Get a specific post
router.get('/:id', async (req, res) => {
  try {
    let mPostId = req.params.id;
    console.log(`GET: View :${mPostId} post.`);

    const connection = await connect(dbConfig);

    let request = new Request(connection);
    request.input('id', req.params.id);

    let result = await request.query(queries.getPostByIdQuery);
    res.json(result.recordset);
  } catch (err) {
    console.error('Database connection failed:', err);
    res.status(500).send('Database connection failed');
  }
  finally
  {
    connect.close();
  }
});

// Create a new post
router.post('/', async (req, res) => {
  try {
    console.log('POST: Create new post.');
    const connection = await connect(dbConfig);
    let request = new Request(connection);

    request.input('reqDate', req.body.reqDate);
    request.input('reqTime', req.body.reqTime);
    request.input('reqSts',  req.body.reqSts);
    request.input('exeComp', req.body.exeComp);
    request.input('reqComp', req.body.reqComp);
    request.input('reqUserName', req.body.reqUserName);
    request.input('reqContents', req.body.reqContents);
    request.input('prcsContents', req.body.prcsContents);
    request.input('prcsMin', req.body.prcsMin);
    request.input('completeReqDate', req.body.completeReqDate);
    request.input('prcsCompleteDate', req.body.prcsCompleteDate);

    let result = await request.query(queries.createPostQuery);
    res.send(result);
  } catch (err) {
    console.error('Database connection failed:', err);
    res.status(500).send('Database connection failed');
  } 
  finally
  {
    connect.close();
  }
});

// Delete a post
router.delete('/:id', async (req, res) => {
  try {
    let mPostId = req.params.id;

    console.log(`DELETE: Delete ${mPostId} post.`);
    const connection = await connect(dbConfig);
    let request = new Request(connection);
    
    request.input('id', req.params.id);
    
    let result = await request.query(queries.deletePostQuery);
    res.send(result);
  } catch (err) {
    console.error('Database connection failed:', err);
    res.status(500).send('Database connection failed');
  }
  finally
  {
    connect.close();
  }
});

// update post
router.put('/:id', async (req, res) => {
  try {
    let mPostId = req.params.id;
    console.log(`PUT: Update :${mPostId} post.`);
    const connection = await connect(dbConfig);
    let request = new Request(connection);

    //serial_no in url
    request.input('id', req.params.id)

    //input params (json) 
    request.input('reqDate', req.body.reqDate);
    request.input('reqTime', req.body.reqTime);
    request.input('reqSts',  req.body.reqSts);
    request.input('exeComp', req.body.exeComp);
    request.input('reqComp', req.body.reqComp);
    request.input('reqUserName', req.body.reqUserName);
    request.input('reqContents', req.body.reqContents);
    request.input('prcsContents', req.body.prcsContents);
    request.input('prcsMin', req.body.prcsMin);
    request.input('completeReqDate', req.body.completeReqDate);
    request.input('prcsCompleteDate', req.body.prcsCompleteDate);

    let result = await request.query(queries.updatePostQuery);
    res.send(result);

  } catch (err) {
    console.error('Database connection failed:', err);
    res.status(500).send('Database connection failed');
  }
  finally
  {
    connect.close();
  }
})

//////CKS////// 추가중
router.get('/api', async (req, res) => {
  try {
    await sql.connect(config);
    //const result = await sql.query("SELECT * FROM TEST_TB");
    const result = await sql.query("SELECT * FROM TB_ADM_USER_MST");
    res.json(result.recordset);

    // console.log(JSON.stringify(myObject)); // 이렇게 사용하세요
    // console.log("JSON_TEST중");
  } 
  catch (error) 
  {
    console.error('Error fetching data:', error.message);
    res.status(500).send('Internal Server Error');
  } 
  finally 
  {
    sql.close();
  }
});

module.exports = router;